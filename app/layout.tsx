import type { Metadata } from "next";
import { Inter } from "next/font/google";
import "./globals.css";
import NavBar from "./components/NavBar";
import { Suspense } from "react";
import Loading from "./loading";
import AuthContext from "./context/AuthContext";

const inter = Inter({ subsets: ["latin"] });

export const metadata: Metadata = {
  title: "Nuravour",
  description: "Find Local Halal Restaurant",
  icons: {
    icon: "./favicon.ico",
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <body className={inter.className}>
        <AuthContext>
          <main className="min-h-screen bg-brand-primary-50">
            <NavBar />
            <Suspense fallback={<Loading />}>{children}</Suspense>
          </main>
        </AuthContext>
      </body>
    </html>
  );
}
