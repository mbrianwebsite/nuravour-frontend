import Header from "./components/Header";
import HomeGrid from "./components/HomeGrid";
import { Cuisine, PRICE, Location, Review } from "@prisma/client";
import prisma from "@/db";

export interface RestaurantsType {
  id: number;
  name: string;
  main_image: string;
  description: string;
  open_time: string;
  close_time: string;
  slug: string;
  price: PRICE;
  location: Location;
  cuisine: Cuisine;
  reviews: Review[];
}

const fetchData = async (): Promise<RestaurantsType[]> => {
  const restaurants = await prisma.restaurant.findMany({
    select: {
      id: true,
      name: true,
      main_image: true,
      description: true,
      open_time: true,
      close_time: true,
      slug: true,
      price: true,
      location: true,
      cuisine: true,
      reviews: true,
    },
  });
  return restaurants;
};

export default async function Home() {
  const restaurant = await fetchData();
  return (
    <>
      <Header />
      <HomeGrid restaurants={restaurant} />
    </>
  );
}
