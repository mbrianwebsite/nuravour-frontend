import ReserveForm from "@/app/components/ReserveForm";
import { Metadata } from "next";
import prisma from "@/db";
import { notFound } from "next/navigation";

export const metadata: Metadata = {
  title: "Reserve | Nuravour",
};

const fetchRestaurantBySlug = async (slug: string) => {
  const restaurant = await prisma.restaurant.findUnique({
    where: { slug },
  });

  if (!restaurant) {
    notFound();
  }

  return restaurant;
};

export default async function Reserve({
  params,
  searchParams,
}: {
  params: { slug: string };
  searchParams: { day: string; time: string; partySize: string };
}) {
  const restaurant = await fetchRestaurantBySlug(params.slug);
  const timeFormat = (times: string) => {
    let time = times.split(":");
    let ampm = "AM";
    if (Number(time[0]) > 12) ampm = "PM";
    return time[0] + ":" + time[1];
  };
  const dateFormat = (date: any) => {
    const newDate = new Date(date);
    const formatedDate = newDate.toUTCString().split(" ");
    return (
      formatedDate[0] +
      " " +
      formatedDate[1] +
      " " +
      formatedDate[2] +
      " " +
      formatedDate[3]
    );
    // return newDate.toUTCString();
  };
  return (
    <div className="mt-10 flex w-screen items-center justify-center pb-10">
      <div className="flex max-w-5xl flex-col gap-4 rounded-md bg-white p-4 md:p-8 lg:p-10">
        <div className="flex flex-col gap-4">
          <div className="text-xl font-semibold text-brand-primary-900">
            You are almost done
          </div>
          <div className="flex flex-row items-center gap-4 rounded-md border-2 border-brand-primary-100 p-3">
            <img
              className="aspect-square w-28 rounded-md object-cover"
              src={restaurant.main_image}
              alt=""
            />
            <div className="flex h-full w-full flex-col justify-between text-brand-primary-900">
              <div className="flex flex-col gap-2">
                <div className="line-clamp-2 py-1 text-lg font-bold leading-none">
                  {restaurant.name}
                </div>
                <div className="leading-none">
                  {searchParams.partySize} People
                </div>
                <div className="leading-none">
                  {dateFormat(searchParams.day)}
                </div>
                <div className="leading-none">
                  {timeFormat(searchParams.time)}
                </div>
              </div>
            </div>
          </div>
        </div>
        <ReserveForm
          day={searchParams.day}
          time={searchParams.time}
          partySize={searchParams.partySize}
          slug={params.slug}
        />
      </div>
    </div>
  );
}
