"use client";

import Button from "../components/Button";
import { useRouter } from "next/navigation";

export default function Error({ error }: { error: Error }) {
  const router = useRouter();
  return (
    <div className="flex h-screen w-full items-center justify-center bg-brand-primary-50 px-16 md:px-0">
      <div className="flex flex-col items-center justify-center rounded-lg border border-brand-primary-300 bg-white px-4 py-8 shadow-2xl md:px-8 lg:px-24">
        <p className="text-6xl font-bold tracking-wider text-brand-primary-600 md:text-7xl lg:text-9xl">
          404
        </p>
        <p className="mt-4 text-2xl font-bold tracking-wider text-brand-primary-600 md:text-3xl lg:text-5xl">
          Restaurant not found.
        </p>
        <p className="mt-4 border-b-2 border-brand-primary-300 pb-4 text-center text-brand-primary-600">
          Cannot find a Restaurant
        </p>
        <div className="py-4">
          <Button
            label="Return to Home"
            style="primary"
            handleOnClick={() => router.push("/")}
          />
        </div>
      </div>
    </div>
  );
}
