import RestaurantMenuBar from "../../components/RestaurantMenuBar";
import RestaurantOverview from "../../components/RestaurantOverview";
import { Metadata } from "next";

import { Location, PRICE } from "@prisma/client";
import RestaurantReservationCard from "@/app/components/RestaurantReservationCard";
import { notFound } from "next/navigation";

import prisma from "@/db";

export interface RestaurantType {
  main_image: string;
  name: string;
  price: PRICE;
  location: Location;
  description: string;
  open_time: string;
  close_time: string;
  slug: string;
}

const fetchRestaurant = async (slug: string): Promise<RestaurantType> => {
  const restaurant = await prisma.restaurant.findUnique({
    where: {
      slug,
    },
    select: {
      main_image: true,
      name: true,
      price: true,
      location: true,
      description: true,
      open_time: true,
      close_time: true,
      slug: true,
    },
  });
  if (!restaurant) {
    notFound();
  }

  return restaurant;
};

const sumReview = async (slug: string) => {
  const reviewData = await prisma.review.aggregate({
    where: {
      restaurant: { slug: slug },
    },
    _sum: { rating: true },
    _count: { rating: true },
  });

  if (!reviewData) throw new Error();

  return reviewData;
};

export const metadata: Metadata = {
  title: "Sultan's Oasis | Nuravour",
  description: "Experience the richness of Middle Eastern flavors",
};

export default async function RestaurantLayout({
  children,
  params,
}: {
  children: React.ReactNode;
  params: { slug: string };
}) {
  const restaurantData = await fetchRestaurant(params.slug);
  const restaurantSumReviewData = await sumReview(params.slug);

  const rating =
    Number(restaurantSumReviewData._sum.rating) /
    Number(restaurantSumReviewData._count.rating);
  return (
    <div className="flex flex-col">
      <img
        className="h-96 w-screen object-cover"
        src={restaurantData.main_image}
        alt=""
      />
      <div className="mx-auto -mt-8 flex w-full max-w-7xl flex-col gap-4 rounded-lg px-4 md:flex-row md:px-8">
        <div className="flex flex-col rounded-md  bg-white p-4 md:w-2/3 md:p-8">
          <RestaurantMenuBar slug={params.slug} />
          <RestaurantOverview overview={restaurantData} rating={rating} />
          {children}
        </div>
        <RestaurantReservationCard
          openTime={restaurantData.open_time}
          closeTime={restaurantData.close_time}
          slug={restaurantData.slug}
        />
      </div>
    </div>
  );
}
