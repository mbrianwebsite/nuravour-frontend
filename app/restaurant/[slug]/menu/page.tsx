import RestaurantMenus from "@/app/components/RestaurantMenus";
import { Metadata } from "next";

import { Item } from "@prisma/client";

import prisma from "@/db";

interface RestaurantType {
  items: Item[];
  main_image: string;
}

const fetchRestaurant = async (slug: string): Promise<RestaurantType> => {
  const restaurant = await prisma.restaurant.findUnique({
    where: {
      slug,
    },
    select: {
      main_image: true,
      items: true,
    },
  });

  if (!restaurant) throw new Error();

  return restaurant;
};

export const metadata: Metadata = {
  title: "Sultan's Oasis (Menu) | Nuravour",
};

export default async function RestaurantMenu({
  params,
}: {
  params: { slug: string };
}) {
  const restaurantData = await fetchRestaurant(params.slug);

  return (
    <>
      <RestaurantMenus menus={restaurantData.items} />
    </>
  );
}
