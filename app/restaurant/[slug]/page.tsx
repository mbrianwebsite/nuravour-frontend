import RestaurantMenus from "@/app/components/RestaurantMenus";
import RestaurantPhotos from "@/app/components/RestaurantPhotos";
import RestaurantReviews from "@/app/components/RestaurantReviews";

import { Item, Review } from "@prisma/client";

import prisma from "@/db";

interface RestaurantType {
  items: Item[];
  main_image: string;
  reviews: Review[];
}

const fetchRestaurant = async (slug: string): Promise<RestaurantType> => {
  const restaurant = await prisma.restaurant.findUnique({
    where: {
      slug,
    },
    select: {
      main_image: true,
      items: true,
      reviews: true,
    },
  });

  if (!restaurant) {
    throw new Error("Restaurant not found!");
  }

  return restaurant;
};

export default async function Restaurant({
  params,
}: {
  params: { slug: string };
}) {
  const restaurantData = await fetchRestaurant(params.slug);

  return (
    <>
      <RestaurantPhotos
        items={restaurantData.items}
        mainImage={restaurantData.main_image}
      />
      <RestaurantMenus menus={restaurantData.items} />
      <RestaurantReviews reviews={restaurantData.reviews} />
    </>
  );
}
