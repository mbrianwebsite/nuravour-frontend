import RestaurantReviews from "@/app/components/RestaurantReviews";
import { Review } from "@prisma/client";
import { Metadata } from "next";

export const metadata: Metadata = {
  title: "Sultan's Oasis (Review) | Nuravour",
};

import prisma from "@/db";

const fetchReview = async (slug: string): Promise<Review[]> => {
  const review = await prisma.review.findMany({
    where: {
      restaurant: { slug: slug },
    },
  });

  if (!review) throw new Error();

  return review;
};

export default async function RestaurantReview({
  params,
}: {
  params: { slug: string };
}) {
  const restaurantReviewData = await fetchReview(params.slug);
  return (
    <>
      <RestaurantReviews reviews={restaurantReviewData} />
    </>
  );
}
