import { Cuisine, Location, PRICE } from "@prisma/client";
import Link from "next/link";

interface SearchFilterType {
  fetchCuisine: () => Promise<Cuisine[]>;
  fetchLocation: () => Promise<Location[]>;
  searchParams: { city?: string; cuisine?: string; price?: PRICE };
}

export default async function SearchFilter({
  fetchCuisine,
  fetchLocation,
  searchParams,
}: SearchFilterType) {
  const cuisine = await fetchCuisine();
  const location = await fetchLocation();

  const prices = [
    { price: PRICE.CHEAP, label: "$" },
    { price: PRICE.REGULAR, label: "$$" },
    { price: PRICE.EXPENSIVE, label: "$$$" },
  ];
  return (
    <div className="flex flex-col gap-4 p-4 sm:p-8">
      <div className="flex flex-col gap-2">
        <div className="font-bold text-brand-primary-900">Location</div>
        <div className="flex flex-col gap-1 font-medium text-brand-primary-900">
          {location &&
            location.map((l, index) => {
              return (
                <Link
                  href={{
                    pathname: "/search",
                    query: { ...searchParams, city: l.name },
                  }}
                  key={index}
                  replace={true}
                >
                  {l.name}
                </Link>
              );
            })}
        </div>
      </div>
      <div className="flex flex-col gap-2">
        <div className="font-bold text-brand-primary-900">Cuisine</div>
        <div className="flex flex-col gap-1 font-medium text-brand-primary-900">
          {cuisine &&
            cuisine.map((c, index) => {
              return (
                <Link
                  href={{
                    pathname: "/search",
                    query: { ...searchParams, cuisine: c.name },
                  }}
                  key={index}
                  replace={true}
                >
                  {c.name}
                </Link>
              );
            })}
        </div>
      </div>
      <div className="flex flex-col gap-2">
        <div className="font-bold text-brand-primary-900">Price</div>
        <div className="flex flex-row gap-1 font-bold text-brand-primary-900 [&>*]:rounded [&>*]:border [&>*]:border-brand-primary-800 [&>*]:px-2 [&>*]:py-1">
          {prices.map((p, index) => {
            return (
              <Link
                key={index}
                href={{
                  pathname: "/search",
                  query: { ...searchParams, price: p.price },
                }}
                replace={true}
              >
                {p.label}
              </Link>
            );
          })}
        </div>
      </div>
    </div>
  );
}
