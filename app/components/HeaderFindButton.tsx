"use client";
import Button from "./Button";
import { useRouter } from "next/navigation";

export default function HeaderFindButton({ keyword }: { keyword: string }) {
  const router = useRouter();
  return (
    <Button
      label="Find"
      style="primary"
      handleOnClick={() => router.push("/search?city=" + keyword)}
    />
  );
}
