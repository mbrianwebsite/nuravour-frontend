"use client";
import Button from "./Button";
import { useRouter } from "next/navigation";

export default function RestaurantBookButton({
  handleClickButton,
}: {
  handleClickButton: () => void;
}) {
  const router = useRouter();
  const handleBookNow = () => {
    handleClickButton();
    // router.push("/reserve/1");
  };
  return (
    <Button style="primary" label="Find a time" handleOnClick={handleBookNow} />
  );
}
