import { FaStar } from "react-icons/fa";
import Link from "next/link";
import { RestaurantsType } from "../page";

import prisma from "@/db";

interface HomeCardType {
  restaurant: RestaurantsType;
}

const sumReview = async (slug: string) => {
  const reviewData = await prisma.review.aggregate({
    where: {
      restaurant: { slug: slug },
    },
    _sum: { rating: true },
    _count: { rating: true },
  });

  if (!reviewData) throw new Error();

  return reviewData;
};

export default async function HomeCard({ restaurant }: HomeCardType) {
  const restaurantSumReviewData = await sumReview(restaurant.slug);

  const rating =
    Number(restaurantSumReviewData._sum.rating) /
    Number(restaurantSumReviewData._count.rating);

  return (
    <Link
      href={"/restaurant/" + restaurant.slug}
      className="flex flex-col rounded-md shadow-xl"
    >
      <img
        className="aspect-video h-32 rounded-t-md bg-brand-primary-900 object-cover"
        src={restaurant.main_image}
        alt="image"
      />
      <div className="flex flex-col rounded-b-md p-3">
        <div className="font-bold text-brand-primary-900">
          {restaurant.name}
        </div>
        <div className="flex flex-col gap-1">
          <div className="flex flex-row items-center gap-2 ">
            <div className="flex flex-row">
              <FaStar
                className={
                  rating >= 1 ? "fill-amber-500" : "fill-brand-primary-800"
                }
              />
              <FaStar
                className={
                  rating >= 2 ? "fill-amber-500" : "fill-brand-primary-800"
                }
              />
              <FaStar
                className={
                  rating >= 3 ? "fill-amber-500" : "fill-brand-primary-800"
                }
              />
              <FaStar
                className={
                  rating >= 4 ? "fill-amber-500" : "fill-brand-primary-800"
                }
              />
              <FaStar
                className={
                  rating == 5 ? "fill-amber-500" : "fill-brand-primary-800"
                }
              />
            </div>
            <div className="text-sm font-medium text-brand-primary-600">
              {restaurantSumReviewData._count.rating} Reviews
            </div>
          </div>
          <div className="flex flex-row items-center gap-1 text-sm text-brand-primary-800 ">
            <div>{restaurant.cuisine.name}</div>
            <div>&#183;</div>
            <div className="flex flex-row ">
              <span className="font-bold text-amber-500">$</span>
              <span
                className={
                  "font-bold " +
                  (restaurant.price === "REGULAR" ||
                  restaurant.price === "EXPENSIVE"
                    ? "text-amber-500"
                    : "text-brand-primary-700")
                }
              >
                $
              </span>
              <span
                className={
                  "font-bold " +
                  (restaurant.price === "EXPENSIVE"
                    ? "text-amber-500"
                    : "text-brand-primary-700")
                }
              >
                $
              </span>
            </div>
            <div>&#183;</div>
            <div>{restaurant.location.name}</div>
          </div>
          <div className="text-sm font-medium text-brand-primary-800">
            Booked 76x
          </div>
        </div>
      </div>
    </Link>
  );
}
