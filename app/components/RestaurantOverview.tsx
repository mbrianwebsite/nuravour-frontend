import { FaComment, FaDollarSign, FaMapPin, FaStar } from "react-icons/fa";
import { RestaurantType } from "../restaurant/[slug]/layout";

interface RestaurantOverviewType {
  overview: RestaurantType;
  rating: number;
}

export default function RestaurantOverview({
  overview,
  rating,
}: RestaurantOverviewType) {
  return (
    <div className="flex flex-col gap-3 border-b-2 border-brand-primary-300 py-8 text-brand-primary-950">
      <div className="text-4xl font-bold text-brand-primary-900">
        {overview.name}
      </div>
      <div className="flex flex-row items-center gap-3">
        <div className="flex flex-row items-center gap-1">
          <div className="flex flex-row items-center">
            <FaStar
              className={
                rating >= 1 ? "fill-amber-500" : "fill-brand-primary-800"
              }
            />
            <FaStar
              className={
                rating >= 2 ? "fill-amber-500" : "fill-brand-primary-800"
              }
            />
            <FaStar
              className={
                rating >= 3 ? "fill-amber-500" : "fill-brand-primary-800"
              }
            />
            <FaStar
              className={
                rating >= 4 ? "fill-amber-500" : "fill-brand-primary-800"
              }
            />
            <FaStar
              className={
                rating == 5 ? "fill-amber-500" : "fill-brand-primary-800"
              }
            />
          </div>
          <div className="font-medium text-brand-primary-900">
            {rating.toFixed(1)}
          </div>
        </div>
        <div className="flex flex-row gap-3 font-medium text-brand-primary-900 [&>div]:flex [&>div]:items-center [&>div]:gap-1">
          <div className="">
            <FaComment className="fill-brand-primary-900" />
            <span>3</span>
          </div>
          <div className="">
            <FaMapPin className="fill-brand-primary-900" />
            <span>Jakarta</span>
          </div>
          <div className="text-lg font-bold text-amber-500">
            <span>$</span>
            {overview.price === "REGULAR" || overview.price === "EXPENSIVE" ? (
              <span>$</span>
            ) : (
              <span className="text-brand-primary-900">$</span>
            )}
            {overview.price === "EXPENSIVE" ? (
              "$"
            ) : (
              <span className="text-brand-primary-900">$</span>
            )}
          </div>
        </div>
      </div>
      <div>{overview.description}</div>
    </div>
  );
}
