interface InputProps {
  children?: React.ReactNode;
  name: string;
  moreClass?: string;
  selectOption: SelectTimeOptionData[];
  widhtClass?: string;
  style: "primary" | "secondary";
  value: any;
  onChange: (e: any) => void;
}

export interface SelectTimeOptionData {
  time: string;
  displayTime: string;
}

export default function SelectTime({
  children,
  moreClass,
  name,
  selectOption,
  widhtClass,
  style,
  onChange,
  value,
}: InputProps) {
  return (
    <div
      className={
        "relative flex flex-row items-center gap-2 rounded-md bg-white px-3 py-2" +
        " " +
        (style === "primary" && "border border-brand-primary-900") +
        " " +
        (widhtClass ? widhtClass : "w-full")
      }
    >
      {children}
      <select
        name={name}
        className={
          "font-medium text-brand-primary-900" +
          " " +
          (widhtClass ? widhtClass : "w-full")
        }
        value={value}
        onChange={onChange}
      >
        {selectOption.map((o: any, index: number) => {
          return (
            <option key={index} value={o.time}>
              {o.displayTime}
            </option>
          );
        })}
      </select>
    </div>
  );
}
