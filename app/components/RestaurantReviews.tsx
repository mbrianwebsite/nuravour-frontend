import { Review } from "@prisma/client";
import { FaStar } from "react-icons/fa";

interface RestaurantReviewsType {
  reviews: Review[];
}

export default function RestaurantReviews({ reviews }: RestaurantReviewsType) {
  return (
    <div className="flex flex-col gap-3 border-b-2 border-brand-primary-300 py-4 text-brand-primary-900">
      <div className="text-xl font-semibold text-brand-primary-900">
        Reviews
      </div>
      <div className="grid grid-cols-1 gap-3 rounded-md lg:grid-cols-2">
        {reviews.map((r, index) => {
          return (
            <div
              key={index}
              className="flex flex-col rounded-md border-2 border-brand-primary-100 p-3 "
            >
              <div className="flex flex-row items-center gap-2 text-sm font-semibold text-brand-primary-900">
                <div className="flex flex-row">
                  <FaStar
                    className={
                      r.rating >= 1
                        ? "fill-amber-500"
                        : "fill-brand-primary-800"
                    }
                  />
                  <FaStar
                    className={
                      r.rating >= 2
                        ? "fill-amber-500"
                        : "fill-brand-primary-800"
                    }
                  />
                  <FaStar
                    className={
                      r.rating >= 3
                        ? "fill-amber-500"
                        : "fill-brand-primary-800"
                    }
                  />
                  <FaStar
                    className={
                      r.rating >= 4
                        ? "fill-amber-500"
                        : "fill-brand-primary-800"
                    }
                  />
                  <FaStar
                    className={
                      r.rating == 5
                        ? "fill-amber-500"
                        : "fill-brand-primary-800"
                    }
                  />
                </div>
                <div>2 Days ago</div>
              </div>
              <div className="py-3 text-lg">{r.text}</div>
              <div className="text-right font-semibold">
                {r.first_name + " " + r.last_name}
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
