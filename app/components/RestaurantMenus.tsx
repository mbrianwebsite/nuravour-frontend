import { Item } from "@prisma/client";

export default function RestaurantMenus({ menus }: { menus: Item[] }) {
  return (
    <div className="flex flex-col gap-3 border-b-2 border-brand-primary-300 py-4">
      <div className="text-xl font-semibold text-brand-primary-900">Menu</div>
      <div className="grid grid-cols-1 gap-3 rounded-md lg:grid-cols-2">
        {menus.map((m) => {
          return (
            <div
              className="flex flex-row items-center gap-4 rounded-md border-2 border-brand-primary-100 p-3"
              key={m.id}
            >
              <img
                className="aspect-square w-28 rounded-md object-cover"
                src={m.image}
                alt=""
              />
              <div className="flex h-full w-full flex-col justify-between text-brand-primary-900">
                <div className="flex flex-col gap-2">
                  <div className="line-clamp-2 text-lg font-bold leading-none">
                    {m.name}
                  </div>
                  <div className="line-clamp-2 leading-none">
                    {m.description}
                  </div>
                </div>
                <div className="text-right text-lg font-bold">{m.price}</div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
}
