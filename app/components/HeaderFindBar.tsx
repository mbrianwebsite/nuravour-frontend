"use client";
import { FaSearch } from "react-icons/fa";
import Input from "./Input";
import HeaderFindButton from "./HeaderFindButton";
import { useState } from "react";

export default function HeaderFindBar() {
  const [keyword, setKeyword] = useState("");
  const handleSetKeyword = (e: any) => {
    setKeyword(e);
  };
  return (
    <div className="flex flex-col gap-2 sm:flex-row ">
      <Input
        name="search"
        label="Location / City"
        type="text"
        style="secondary"
        value={keyword}
        setValue={(e) => handleSetKeyword(e.target.value)}
      >
        <FaSearch className="fill-brand-primary-900" />
      </Input>
      <HeaderFindButton keyword={keyword} />
    </div>
  );
}
