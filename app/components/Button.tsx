"use client";

interface ButtonProps {
  label: string;
  handleOnClick?: () => void;
  style: "primary" | "secondary";
  disabled?: boolean;
  widthClass?: string;
}

export default function Button({
  label,
  handleOnClick,
  style,
  disabled,
  widthClass,
}: ButtonProps) {
  const buttonClass =
    "px-4 py-2 font-medium text-base rounded-md min-w-[100px] text-center flex items-center justify-center disabled:cursor-not-allowed";
  return (
    <button
      onClick={handleOnClick}
      className={
        buttonClass +
        " " +
        (style === "primary"
          ? "bg-brand-primary-900 text-brand-primary-100 disabled:bg-brand-primary-300 disabled:text-brand-primary-500"
          : "border border-brand-primary-900 bg-brand-primary-100 text-brand-primary-900") +
        " " +
        (widthClass && widthClass)
      }
      disabled={disabled}
    >
      {label}
    </button>
  );
}
