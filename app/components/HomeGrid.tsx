import HomeCard from "./HomeCard";

import { RestaurantsType } from "../page";

interface HomeGridProps {
  restaurants: RestaurantsType[];
}

export default function HomeGrid({ restaurants }: HomeGridProps) {
  return (
    <div className="mx-auto flex max-w-7xl flex-col gap-6 p-4 md:p-8">
      <div className="flex flex-row items-center justify-between">
        <div className="text-xl font-semibold text-brand-primary-900">
          Availible Now
        </div>
        <div className="font-medium text-brand-primary-900">View More</div>
      </div>
      <div className="grid grid-cols-1 justify-center gap-4 sm:grid-cols-2 lg:grid-cols-3">
        {restaurants.map((restaurant, index) => (
          <HomeCard restaurant={restaurant} key={index} />
        ))}
      </div>
    </div>
  );
}
