"use client";

interface InputProps {
  children?: React.ReactNode;
  type: "text" | "date" | "number" | "password" | "time" | "email";
  name: string;
  label?: string;
  handleOnEnter?: () => void;
  moreClass?: string;
  widhtClass?: string;
  style: "primary" | "secondary";
  value: any;
  setValue: (e: any) => void;
}

export default function Input({
  children,
  label,
  name,
  handleOnEnter,
  type,
  moreClass,
  widhtClass,
  style,
  value,
  setValue,
}: InputProps) {
  const onEnter = (e: any) => {
    if (e.key === "Enter") {
      if (handleOnEnter) {
        handleOnEnter();
      }
    }
  };

  return (
    <div
      className={
        "relative flex flex-row items-center gap-2 rounded-md bg-white px-3 py-2" +
        " " +
        (style === "primary" && "border border-brand-primary-900") +
        " " +
        (widhtClass ? widhtClass : "max-w-[300px]")
      }
    >
      {children}
      <input
        name={name}
        className={
          "bg-white font-medium text-brand-primary-900 " +
          " " +
          (style === "primary" && "bg-brand-primary-100") +
          " " +
          (widhtClass ? widhtClass : "max-w-[300px]") +
          " " +
          moreClass
        }
        type={type}
        placeholder={label}
        onKeyUp={onEnter}
        value={value}
        onChange={setValue}
        autoComplete="true"
      />
    </div>
  );
}
