"use client";

import { MdOutlineWbTwilight } from "react-icons/md";
import Button from "./Button";
import { FaBars, FaTimes } from "react-icons/fa";
import { useRouter } from "next/navigation";
import { useContext, useEffect, useState } from "react";
import Input from "./Input";
import useAuth from "@/hooks/useAuth";
import { AuthenticationContext } from "../context/AuthContext";
import LoadingButton from "./LoadingButton";

export default function NavBar() {
  const router = useRouter();

  const [signUpModal, setSignUpModal] = useState(false);
  const [signInModal, setSignInModal] = useState(false);
  const [disabled, setDisabled] = useState(true);

  const { signin, signup } = useAuth();

  const { error, loading, data, setAuthState } = useContext(
    AuthenticationContext,
  );

  const [inputs, setInputs] = useState({
    email: "",
    firstName: "",
    lastName: "",
    password: "",
    city: "",
    phone: "",
  });

  const handleChangeInputs = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
  };

  const handleOnClickButton = () => {
    if (signInModal) {
      signin(
        { email: inputs.email, password: inputs.password },
        closeModalSignIn,
      );
    }
    if (signUpModal) {
      signup(
        {
          email: inputs.email,
          password: inputs.password,
          firstName: inputs.firstName,
          lastName: inputs.lastName,
          city: inputs.city,
          phone: inputs.phone,
        },
        closeModalSignUp,
      );
    }
  };

  const closeModalSignUp = () => {
    setSignUpModal(false);
    setInputs({
      email: "",
      firstName: "",
      lastName: "",
      password: "",
      city: "",
      phone: "",
    });
  };
  const closeModalSignIn = () => {
    setSignInModal(false);
    setInputs({
      email: "",
      firstName: "",
      lastName: "",
      password: "",
      city: "",
      phone: "",
    });
  };

  const { signout } = useAuth();

  useEffect(() => {
    if (signInModal) {
      if (inputs.password && inputs.email) {
        return setDisabled(false);
      }
    }
    if (signUpModal) {
      if (
        inputs.password &&
        inputs.email &&
        inputs.firstName &&
        inputs.lastName &&
        inputs.city &&
        inputs.phone
      ) {
        return setDisabled(false);
      }
    }
    setDisabled(true);
  }, [inputs]);
  return (
    <>
      <div className="bg-white">
        <div className="mx-auto flex max-w-7xl flex-row items-center justify-between bg-white p-4 md:p-8">
          <div className="flex flex-row items-center gap-1 font-bold text-brand-primary-900">
            <div className="pr-2 md:hidden">
              <FaBars className="h-8 w-8 rounded-md fill-brand-primary-900 p-2 ring-2 ring-brand-primary-900" />
            </div>
            <div
              className="flex flex-row items-center gap-1"
              onClick={() => router.push("/")}
            >
              <div>
                <MdOutlineWbTwilight className="h-9 w-9 fill-brand-primary-900" />
              </div>
              <div className="text-2xl">Nuravour</div>
            </div>
          </div>
          <div className="hidden flex-row items-center gap-2 md:flex">
            {loading ? null : data ? (
              <>
                <div className="font-bold text-brand-primary-900">
                  Hy, {data.firstName} {data.lastName}
                </div>
                <Button
                  label="Sign Out"
                  style="secondary"
                  handleOnClick={() => signout()}
                />
              </>
            ) : (
              <>
                <Button
                  label="Sign Up"
                  style="primary"
                  handleOnClick={() => setSignUpModal(true)}
                />
                <Button
                  label="Sign In"
                  style="secondary"
                  handleOnClick={() => setSignInModal(true)}
                />
              </>
            )}
          </div>
        </div>
      </div>
      {signUpModal && (
        <div className="fixed left-0 top-0 z-10  bg-[rgba(0,0,0,0.5)]">
          <div className="flex h-screen w-screen items-center justify-center">
            <div className="flex flex-col gap-4 rounded-lg bg-white p-4 shadow-2xl md:p-8">
              <div className="flex flex-row items-center justify-between text-2xl font-bold text-brand-primary-900">
                <div className="">Sign Up</div>
                <div onClick={() => closeModalSignUp()}>
                  <FaTimes className="fill-brand-primary-900" />
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
                  <Input
                    name="firstName"
                    type="text"
                    style="primary"
                    label="First Name"
                    value={inputs.firstName}
                    setValue={handleChangeInputs}
                  />
                  <Input
                    name="lastName"
                    type="text"
                    style="primary"
                    label="Last Name"
                    value={inputs.lastName}
                    setValue={handleChangeInputs}
                  />
                  <Input
                    name="city"
                    type="text"
                    style="primary"
                    label="City"
                    value={inputs.city}
                    setValue={handleChangeInputs}
                  />
                  <Input
                    name="phone"
                    type="text"
                    style="primary"
                    label="Phone Number"
                    value={inputs.phone}
                    setValue={handleChangeInputs}
                  />
                  <Input
                    name="email"
                    type="text"
                    style="primary"
                    label="Email"
                    value={inputs.email}
                    setValue={handleChangeInputs}
                  />
                  <Input
                    name="password"
                    type="password"
                    style="primary"
                    label="Password"
                    value={inputs.password}
                    setValue={handleChangeInputs}
                  />
                </div>
                {error && (
                  <div className="font-medium text-red-600">{error}</div>
                )}
                {!loading ? (
                  <Button
                    label="Sign up"
                    style="primary"
                    disabled={disabled}
                    handleOnClick={handleOnClickButton}
                  />
                ) : (
                  <LoadingButton />
                )}
              </div>
            </div>
          </div>
        </div>
      )}
      {signInModal && (
        <div className="fixed left-0 top-0 z-10  bg-[rgba(0,0,0,0.5)]">
          <div className="flex h-screen w-screen items-center justify-center">
            <div className="flex flex-col gap-4 rounded-lg bg-white p-4 shadow-2xl md:p-8">
              <div className="flex flex-row items-center justify-between text-2xl font-bold text-brand-primary-900">
                <div className="">Sign In</div>
                <div onClick={() => closeModalSignIn()}>
                  <FaTimes className="fill-brand-primary-900" />
                </div>
              </div>
              <div className="flex flex-col gap-2">
                <div className="flex flex-col gap-2">
                  <Input
                    name="email"
                    type="text"
                    style="primary"
                    label="Email"
                    value={inputs.email}
                    setValue={handleChangeInputs}
                  />
                  <Input
                    name="password"
                    type="password"
                    style="primary"
                    label="Password"
                    value={inputs.password}
                    setValue={handleChangeInputs}
                  />
                </div>
                {error && (
                  <div className="font-medium text-red-600">{error}</div>
                )}
                {!loading ? (
                  <Button
                    label="Sign in"
                    style="primary"
                    disabled={disabled}
                    handleOnClick={handleOnClickButton}
                  />
                ) : (
                  <LoadingButton />
                )}
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}
