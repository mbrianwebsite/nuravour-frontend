import Button from "./Button";
import { FaSearch } from "react-icons/fa";
import Input from "./Input";
import { SelectOptionData } from "./Select";
import HeaderFindButton from "./HeaderFindButton";
import HeaderFindBar from "./HeaderFindBar";

export default function Header() {
  let selectOption: SelectOptionData[] = [
    { value: 2, label: "2 Person" },
    { value: 4, label: "4 Person" },
    { value: 6, label: "6 Person" },
    { value: 8, label: "8 Person" },
  ];
  return (
    <div className="bg-gradient-to-tr from-brand-primary-700 via-brand-primary-600 to-brand-primary-500 py-10">
      <div className="mx-auto max-w-7xl p-4 md:p-8 ">
        <div className="flex flex-col gap-6">
          <div className="text-center text-4xl font-bold text-white">
            Find Local Halal Restaurant
          </div>
          <div className="mx-auto flex flex-col gap-2 text-brand-primary-900 md:flex-row ">
            {/* <div className="flex flex-col gap-2 sm:flex-row ">
              <Input name="date" type="date" style="secondary">
                <FaRegCalendarAlt className="fill-brand-primary-900" />
              </Input>
              <Input name="time" type="time" style="secondary">
                <FaRegClock className="fill-brand-primary-900" />
              </Input>
              <Select
                name="people"
                selectOption={selectOption}
                style="secondary"
              >
                <FaUserFriends className="fill-brand-primary-900" />
              </Select>
            </div> */}
            <HeaderFindBar />
          </div>
        </div>
      </div>
    </div>
  );
}
