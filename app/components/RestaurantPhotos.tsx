import { Item } from "@prisma/client";

export default function RestaurantPhotos({
  items,
  mainImage,
}: {
  items: Item[];
  mainImage: string;
}) {
  return (
    <div className="flex flex-col gap-3 border-b-2 border-brand-primary-300 py-4">
      <div className="text-xl font-semibold text-brand-primary-900">Photo</div>
      <div className="grid grid-cols-2 rounded-md">
        <img
          className="aspect-video w-full object-cover"
          src={mainImage}
          alt=""
        />
        {items.map((i: Item, index) => {
          return (
            <img
              key={index}
              className="aspect-video w-full object-cover"
              src={i.image}
              alt=""
            />
          );
        })}
      </div>
    </div>
  );
}
