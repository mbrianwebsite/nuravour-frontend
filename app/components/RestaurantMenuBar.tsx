"use client";

import Link from "next/link";

export default function RestaurantMenuBar({ slug }: { slug: string }) {
  return (
    <div className="flex flex-row items-center gap-4 border-b-2 border-brand-primary-300 text-base font-medium text-brand-primary-900 md:text-lg [&>*]:pb-3">
      <Link
        href={"/restaurant/" + slug}
        className="border-b-2 border-brand-primary-900"
      >
        Overview
      </Link>
      <Link href={"/restaurant/" + slug + "/photo"}>Photo</Link>
      <Link href={"/restaurant/" + slug + "/menu"}>Menu</Link>
      <Link href={"/restaurant/" + slug + "/review"}>Review</Link>
    </div>
  );
}
