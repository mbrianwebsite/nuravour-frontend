interface InputProps {
  children?: React.ReactNode;
  name: string;
  moreClass?: string;
  selectOption: SelectOptionData[];
  widhtClass?: string;
  style: "primary" | "secondary";
  value: any;
  onChange: (e: any) => void;
}

export interface SelectOptionData {
  value: number;
  label: string;
}

export default function Select({
  children,
  moreClass,
  name,
  selectOption,
  widhtClass,
  style,
  value,
  onChange,
}: InputProps) {
  return (
    <div
      className={
        "relative flex flex-row items-center gap-2 rounded-md bg-white px-3 py-2" +
        " " +
        (style === "primary" && "border border-brand-primary-900") +
        " " +
        (widhtClass ? widhtClass : "max-w-[300px]")
      }
    >
      {children}
      <select
        name={name}
        className={
          "font-medium text-brand-primary-900" +
          " " +
          (widhtClass ? widhtClass : "max-w-[300px]")
        }
        value={value}
        onChange={onChange}
      >
        {selectOption.map((o: any, index: number) => {
          return (
            <option key={index} value={o.value}>
              {o.label}
            </option>
          );
        })}
      </select>
    </div>
  );
}
