"use client";

import { FaRegCalendar, FaRegClock, FaUserFriends } from "react-icons/fa";
import Select, { SelectOptionData } from "./Select";
import RestaurantBookButton from "./RestaurantBookButton";
import { useState } from "react";
import "react-datepicker/dist/react-datepicker.css";

import { partySize, times } from "@/data";
import ReactDatePicker from "react-datepicker";
import SelectTime from "./SelectTime";
import useAvailabilities from "@/hooks/useAvailabilities";
import LoadingButton from "./LoadingButton";
import Link from "next/link";

export default function RestaurantReservationCard({
  openTime,
  closeTime,
  slug,
}: {
  openTime: string;
  closeTime: string;
  slug: string;
}) {
  const { data, loading, error, fetchAvailabilities } = useAvailabilities();
  const [selectedDate, setSelectedDate] = useState<Date | null>(new Date());
  const [time, setTime] = useState(openTime);
  const [day, setDay] = useState(new Date().toISOString().split("T")[0]);
  const [partySizeValue, setPartySizeValue] = useState("2");
  const handleChangeDate = (date: Date | null) => {
    if (date) {
      setDay(date.toISOString().split("T")[0]);
      return setSelectedDate(date);
    }
    return setSelectedDate(null);
  };
  const handleClickBookButton = () => {
    fetchAvailabilities({
      slug,
      day,
      time,
      partySize: partySizeValue,
    });
  };

  const timeFormat = (times: string) => {
    let time = times.split(":");
    let ampm = "AM";
    if (Number(time[0]) > 12) ampm = "PM";
    return time[0] + ":" + time[1];
  };

  const filterRestaurantByOpenWindow = () => {
    const timesWithinWindow: typeof times = [];

    let isWithinWindow = false;

    times.forEach((time) => {
      if (time.time === openTime) {
        isWithinWindow = true;
      }
      if (isWithinWindow) {
        timesWithinWindow.push(time);
      }
      if (time.time === closeTime) {
        isWithinWindow = false;
      }
    });
    return timesWithinWindow;
  };
  return (
    <div className="flex h-fit flex-col gap-3 rounded-md bg-white p-4 shadow-md md:w-1/3 md:p-8">
      <div className="text-center text-xl font-semibold text-brand-primary-900">
        Make Reservation
      </div>
      <div>
        <Select
          name="people"
          selectOption={partySize}
          style="primary"
          widhtClass="w-full"
          value={partySizeValue}
          onChange={(e) => setPartySizeValue(e.target.value)}
        >
          <FaUserFriends className="h-6 w-6 fill-brand-primary-900" />
        </Select>
      </div>
      <div className="flex flex-col gap-3 xl:flex-row">
        {/* <Input
          name="date"
          type="date"
          style="primary"
          widhtClass="w-full"
          value={date}
          setValue={(e) => setDate(e.target.value)}
        >
          <FaRegCalendar className="fill-brand-primary-900" />
        </Input> */}
        <div
          className={
            "flex w-full flex-row items-center gap-2 rounded-md border border-brand-primary-900 bg-white px-3 py-2"
          }
        >
          <FaRegCalendar className="h-6 w-6 fill-brand-primary-900" />
          <ReactDatePicker
            selected={selectedDate}
            onChange={handleChangeDate}
            dateFormat="MMMM d"
            className="w-full bg-white font-medium text-brand-primary-900 focus:outline-none "
          />
        </div>
        {/* <Input
          name="time"
          type="time"
          style="primary"
          widhtClass="w-full"
          value={clock}
          setValue={(e) => setClock(e.target.value)}
        >
          <FaRegClock className="fill-brand-primary-900" />
        </Input> */}
        <SelectTime
          name="time"
          selectOption={filterRestaurantByOpenWindow()}
          style="primary"
          widhtClass="w-full"
          value={time}
          onChange={(e) => setTime(e.target.value)}
        >
          <FaRegClock className="h-6 w-6 fill-brand-primary-900" />
        </SelectTime>
      </div>
      {loading ? (
        <LoadingButton />
      ) : (
        <RestaurantBookButton handleClickButton={handleClickBookButton} />
      )}
      {data && data.length ? (
        <div className="mt-2 flex w-full flex-col gap-2">
          <div className="text-base font-medium text-brand-primary-900">
            Select a Time
          </div>
          <div className="grid grid-cols-3 gap-3 sm:grid-cols-4 md:grid-cols-2 lg:grid-cols-3">
            {data.map((time, index) => {
              return (
                time.available && (
                  <Link
                    key={index}
                    className="rounded-md bg-brand-primary-900 px-4 py-2 text-center text-brand-primary-100"
                    href={
                      "/reserve/" +
                      slug +
                      "?day=" +
                      day +
                      "&time=" +
                      time.time +
                      "&partySize=" +
                      partySizeValue
                    }
                  >
                    {timeFormat(time.time)}
                  </Link>
                )
              );
            })}
          </div>
        </div>
      ) : null}
    </div>
  );
}
