import { FaFilter } from "react-icons/fa";
import HomeCard from "./HomeCard";
import { RestaurantsType } from "../page";
import { PRICE } from "@prisma/client";

export default async function SearchGrid({
  searchParams,
  fetchSearch,
}: {
  searchParams: { city?: string; cuisine?: string; price?: PRICE };
  fetchSearch: (
    city?: string | undefined,
    cuisine?: string | undefined,
    price?: PRICE | undefined,
  ) => Promise<RestaurantsType[]>;
}) {
  const searchResult = await fetchSearch(
    searchParams.city,
    searchParams.cuisine,
    searchParams.price,
  );
  return (
    <div className="mx-auto flex max-w-7xl flex-col gap-6 p-4 sm:p-8">
      <div className="flex flex-row items-center justify-between">
        <div className="text-xl font-semibold capitalize text-brand-primary-900">
          Search result "
          {searchParams.price &&
            searchParams.cuisine &&
            searchParams.price.toLowerCase() + " "}
          {searchParams.price &&
            !searchParams.cuisine &&
            searchParams.price.toLowerCase() + " Restaurant "}
          {searchParams.cuisine && searchParams.cuisine + " Restaurant "}
          {searchParams.city ? "in " + searchParams.city : "in All City"}"
        </div>
        <div className="sm:hidden">
          <FaFilter className="h-8 w-8 rounded-md fill-brand-primary-900 p-2 ring-2 ring-brand-primary-900" />
        </div>
      </div>
      <div className="grid grid-cols-1 justify-center gap-4 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
        {searchResult.length
          ? searchResult.map((r, index) => {
              return <HomeCard restaurant={r} key={index} />;
            })
          : "There is No Data"}
      </div>
    </div>
  );
}
