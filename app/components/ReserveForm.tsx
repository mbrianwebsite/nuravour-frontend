"use client";

import { useEffect, useState } from "react";
import Button from "./Button";
import Input from "./Input";
import useReservation from "@/hooks/useReservation";
import LoadingButton from "./LoadingButton";

export default function ReserveForm({
  day,
  time,
  partySize,
  slug,
}: {
  day: string;
  time: string;
  partySize: string;
  slug: string;
}) {
  const [disabled, setDisabled] = useState(true);

  const { createReservation, error, loading } = useReservation();

  const [inputs, setInputs] = useState({
    bookerFirstName: "",
    bookerLastName: "",
    bookerEmail: "",
    bookerPhone: "",
    bookerOccasion: "",
    bookerRequest: "",
  });

  const handleChangeInputs = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputs({
      ...inputs,
      [e.target.name]: e.target.value,
    });
  };

  const handleCompleteReservation = async () => {
    const booking = await createReservation({
      bookerEmail: inputs.bookerEmail,
      bookerFirstName: inputs.bookerFirstName,
      bookerLastName: inputs.bookerLastName,
      bookerOccasion: inputs.bookerOccasion,
      bookerPhone: inputs.bookerPhone,
      bookerRequest: inputs.bookerRequest,
      day,
      partySize,
      slug,
      time,
    });
  };

  useEffect(() => {
    if (
      inputs.bookerFirstName &&
      inputs.bookerLastName &&
      inputs.bookerEmail &&
      inputs.bookerPhone &&
      inputs.bookerOccasion &&
      inputs.bookerRequest
    ) {
      return setDisabled(false);
    }
    setDisabled(true);
  }, [inputs]);

  return (
    <div className="flex flex-col gap-3">
      <div className="grid grid-cols-1 gap-2 md:grid-cols-2">
        <Input
          type="text"
          style="primary"
          name="bookerFirstName"
          value={inputs.bookerFirstName}
          setValue={handleChangeInputs}
          label="First Name"
          widhtClass="w-full"
        />
        <Input
          type="text"
          style="primary"
          name="bookerLastName"
          value={inputs.bookerLastName}
          setValue={handleChangeInputs}
          label="Last Name"
          widhtClass="w-full"
        />
        <Input
          type="text"
          style="primary"
          name="bookerPhone"
          value={inputs.bookerPhone}
          setValue={handleChangeInputs}
          label="Phone Number"
          widhtClass="w-full"
        />
        <Input
          type="email"
          style="primary"
          name="bookerEmail"
          value={inputs.bookerEmail}
          setValue={handleChangeInputs}
          label="Email"
          widhtClass="w-full"
        />
        <Input
          type="text"
          style="primary"
          name="bookerOccasion"
          value={inputs.bookerOccasion}
          setValue={handleChangeInputs}
          label="Occasional"
          widhtClass="w-full"
        />
        <Input
          type="text"
          style="primary"
          name="bookerRequest"
          value={inputs.bookerRequest}
          setValue={handleChangeInputs}
          label="Request"
          widhtClass="w-full"
        />
      </div>
      <div>
        {loading ? (
          <LoadingButton />
        ) : (
          <Button
            style="primary"
            label="Complete Reservation"
            widthClass="w-full"
            disabled={disabled}
            handleOnClick={handleCompleteReservation}
          />
        )}
      </div>
      <div className="font-medium text-red-500">{error && error}</div>
    </div>
  );
}
