import { Metadata } from "next";
import Header from "../components/Header";
import NavBar from "../components/NavBar";
import SearchFilter from "../components/SearchFilter";
import SearchGrid from "../components/SearchGrid";

export const metadata: Metadata = {
  title: "Search",
};

import { Cuisine, Location, PRICE } from "@prisma/client";
import { RestaurantsType } from "../page";

import prisma from "@/db";

const fetchSearch = (
  query?: string | undefined,
  cuisine?: string | undefined,
  price?: PRICE | undefined,
): Promise<RestaurantsType[]> => {
  const select = {
    id: true,
    name: true,
    main_image: true,
    description: true,
    open_time: true,
    close_time: true,
    slug: true,
    price: true,
    location: true,
    cuisine: true,
    reviews: true,
  };

  let whereQuery: any = {};

  if (query) {
    const location = {
      name: {
        equals: query,
      },
    };
    whereQuery.location = location;
  }
  if (cuisine) {
    const cuisineQuery = {
      name: {
        equals: cuisine,
      },
    };
    whereQuery.cuisine = cuisineQuery;
  }
  if (price) {
    const priceQuery = {
      equals: price,
    };
    whereQuery.price = priceQuery;
  }

  return prisma.restaurant.findMany({
    where: whereQuery,
    select,
  });
};

const fetchLocationData = async (): Promise<Location[]> => {
  const locationData = await prisma.location.findMany({});
  if (!locationData) throw new Error();
  return locationData;
};
const fetchCuisineData = async (): Promise<Cuisine[]> => {
  const cuisineData = await prisma.cuisine.findMany({});
  if (!cuisineData) throw new Error();
  return cuisineData;
};

export default function Search({
  searchParams,
}: {
  searchParams: { city?: string; cuisine?: string; price?: PRICE };
}) {
  return (
    <>
      <Header />
      <div className="mx-auto flex w-full max-w-7xl flex-col sm:flex-row">
        <div className="hidden w-fit sm:block">
          <SearchFilter
            fetchCuisine={fetchCuisineData}
            fetchLocation={fetchLocationData}
            searchParams={searchParams}
          />
        </div>
        <div className="w-full">
          <SearchGrid searchParams={searchParams} fetchSearch={fetchSearch} />
        </div>
      </div>
    </>
  );
}
