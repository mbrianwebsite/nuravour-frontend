/** @type {import('next').NextConfig} */
const nextConfig = {
  i18n: {
    locales: ["id"],
    defaultLocale: "id",
  },
};

module.exports = nextConfig;
