import { NextApiRequest, NextApiResponse } from "next";
import jwt from "jsonwebtoken";

import prisma from "@/db";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse,
) {
  if (req.method === "GET") {
    const bearerToken = req.headers.authorization as string;
    const token = bearerToken.split(" ")[1];

    const payload = jwt.decode(token) as { email: string };

    if (!payload.email) {
      return res.status(401).json({
        errorMessage: "Unauthorized request",
      });
    }

    const data = await prisma.user.findUnique({
      where: { email: payload.email },
      select: {
        first_name: true,
        last_name: true,
        email: true,
        city: true,
        phone: true,
      },
    });

    if (!data) {
      return res.status(401).json({
        errorMessage: "No user found!",
      });
    }

    return res.status(200).json({
      firstName: data.first_name,
      lastName: data.last_name,
      email: data.email,
      city: data.city,
      phone: data.phone,
    });
  }

  return res.status(404).json("Unknown endpoint");
}
