// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

import prisma from "@/db";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  await prisma.location.deleteMany();

  await prisma.location.createMany({
    data: [
      { name: "Surabaya" },
      { name: "Malang" },
      { name: "Jember" },
      { name: "Banyuwangi" },
      { name: "Bondowoso" },
      { name: "Probolinggo" },
      { name: "Lumajang" },
      { name: "Situbondo" },
      { name: "Pacitan" },
      { name: "Ponorogo" },
    ],
  });

  res.status(200).json({ name: "Location Successfully Seeded" });
}
