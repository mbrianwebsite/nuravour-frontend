// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/db";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  await prisma.table.deleteMany();

  const restaurants = await prisma.restaurant.findMany();

  const sude =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "sultans-delight-savory-turkish-cuisine",
    )?.id || 1;
  const mahe =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "masala-haven-aromatic-indian-delights",
    )?.id || 1;
  const muma =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "mughlai-magic-exquisite-mughlai-cuisine",
    )?.id || 1;
  const aroa =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "arabian-oasis-authentic-middle-eastern-flavors",
    )?.id || 1;
  const turde =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "turkish-delight-culinary-journey-to-turkey",
    )?.id || 1;
  const mezla =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "mezze-lane-authentic-mediterranean-fare",
    )?.id || 1;
  const safel =
    restaurants.find(
      (restaurant) => restaurant.slug === "saffron-elegance-persian-gastronomy",
    )?.id || 1;
  const marspi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "marrakesh-spice-vibrant-moroccan-flavors",
    )?.id || 1;
  const tajmabi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "taj-mahal-bites-fusion-indian-cuisine",
    )?.id || 1;
  const bosbi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "bosphorus-bites-authentic-turkish-fare",
    )?.id || 1;
  const roymed =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "royal-medina-elegant-moroccan-cuisine",
    )?.id || 1;
  const cabi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "cairo-bistro-egyptian-culinary-journey",
    )?.id || 1;

  const restaurantIds = [
    sude,
    mahe,
    muma,
    aroa,
    turde,
    mezla,
    safel,
    marspi,
    tajmabi,
    bosbi,
    roymed,
    cabi,
  ];

  // Generating data for each restaurant
  const data = [];
  for (const restaurantId of restaurantIds) {
    const numberOfEntries = Math.floor(Math.random() * 3) + 3; // Random between 3 to 5 entries per restaurant

    for (let i = 0; i < numberOfEntries; i++) {
      const seats = [2, 4, 6, 8][Math.floor(Math.random() * 4)]; // Random seat capacity (2, 4, 6, or 8)
      data.push({ restaurant_id: restaurantId, seats });
    }
  }

  // Output the generated data
  await prisma.table.createMany({
    data: data,
  });

  res.status(200).json({ name: "Table Successfully Seeded" });
}
