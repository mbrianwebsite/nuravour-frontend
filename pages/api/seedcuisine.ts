// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/db";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  await prisma.cuisine.deleteMany();

  await prisma.cuisine.createMany({
    data: [
      { name: "Indian" },
      { name: "Arabian" },
      { name: "Turkish" },
      { name: "Moroccan" },
      { name: "Persian" },
      { name: "Egyptian" },
    ],
  });

  res.status(200).json({ name: "Cuisine Successfully Seeded" });
}
