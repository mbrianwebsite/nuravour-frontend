// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";

import prisma from "@/db";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  await prisma.user.deleteMany();

  const userLaith = await prisma.user.create({
    data: {
      first_name: "Laith",
      last_name: "Harb",
      email: "laith@hotmail.com",
      city: "ottawa",
      password: "$2b$10$I8xkU2nQ8EAHuVOdbMy9YO/.rSU3584Y.H4LrpIujGNDtmny9FnLu",
      phone: "1112223333",
    },
  });

  const userJosh = await prisma.user.create({
    data: {
      first_name: "Josh",
      last_name: "Allen",
      email: "josh@hotmail.com",
      city: "toronto",
      password: "$2b$10$I8xkU2nQ8EAHuVOdbMy9YO/.rSU3584Y.H4LrpIujGNDtmny9FnLu",
      phone: "1112223333",
    },
  });

  const userLebron = await prisma.user.create({
    data: {
      first_name: "LeBron",
      last_name: "James",
      email: "lebron@hotmail.com",
      city: "niagara",
      password: "$2b$10$I8xkU2nQ8EAHuVOdbMy9YO/.rSU3584Y.H4LrpIujGNDtmny9FnLu",
      phone: "1112223333",
    },
  });

  const userCassidy = await prisma.user.create({
    data: {
      first_name: "Cassidy",
      last_name: "Marksom",
      email: "cassidy@hotmail.com",
      city: "toronto",
      password: "$2b$10$I8xkU2nQ8EAHuVOdbMy9YO/.rSU3584Y.H4LrpIujGNDtmny9FnLu",
      phone: "1112223333",
    },
  });

  const restaurants = await prisma.restaurant.findMany();

  const sude =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "sultans-delight-savory-turkish-cuisine",
    )?.id || 1;
  const mahe =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "masala-haven-aromatic-indian-delights",
    )?.id || 1;
  const muma =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "mughlai-magic-exquisite-mughlai-cuisine",
    )?.id || 1;
  const aroa =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "arabian-oasis-authentic-middle-eastern-flavors",
    )?.id || 1;
  const turde =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "turkish-delight-culinary-journey-to-turkey",
    )?.id || 1;
  const mezla =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "mezze-lane-authentic-mediterranean-fare",
    )?.id || 1;
  const safel =
    restaurants.find(
      (restaurant) => restaurant.slug === "saffron-elegance-persian-gastronomy",
    )?.id || 1;
  const marspi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "marrakesh-spice-vibrant-moroccan-flavors",
    )?.id || 1;
  const tajmabi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "taj-mahal-bites-fusion-indian-cuisine",
    )?.id || 1;
  const bosbi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "bosphorus-bites-authentic-turkish-fare",
    )?.id || 1;
  const roymed =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "royal-medina-elegant-moroccan-cuisine",
    )?.id || 1;
  const cabi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "cairo-bistro-egyptian-culinary-journey",
    )?.id || 1;

  await prisma.review.createMany({
    data: [
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "This place is amazing, it has some of the best dishes in the world. It is so so so good!!!",
        rating: 5,
        restaurant_id: sude,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "This food is so good! It is the fanciest thing I have ever seen in my short life",
        rating: 5,
        restaurant_id: mahe,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "Excellent food and service. Busy night, but everything was great! Highly recommend.",
        rating: 5,
        restaurant_id: muma,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "Very nice place for a date night, the service was fast and friendly. The food was amazing.",
        rating: 4,
        restaurant_id: aroa,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "Extremely disappointing in our experience.",
        rating: 2,
        restaurant_id: turde,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "This place is amazing, it has some of the best dishes in the world. It is so so so good!!!",
        rating: 5,
        restaurant_id: mezla,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "As always, food was excellent. Waitress was friendly and prompt. We had just one problem in that our dessert order went rogue in the system and we waited ages for it to arrive.",
        rating: 5,
        restaurant_id: safel,
        user_id: userLaith.id,
      },
      {
        first_name: "Laith",
        last_name: "Harb",
        text: "Restaurant was loud and crowded. Food is not worth the price.",
        rating: 3,
        restaurant_id: marspi,
        user_id: userLaith.id,
      },
      {
        first_name: "Josh",
        last_name: "Allen",
        text: "A Christmas lunch with clients served by a friendly server with a good wine selection everyone enjoyed the appetizers and meals",
        rating: 4,
        restaurant_id: tajmabi,
        user_id: userJosh.id,
      },
      {
        first_name: "Josh",
        last_name: "Allen",
        text: "The food was very tasty, the price is a little high so a place to go only for special occasions",
        rating: 5,
        restaurant_id: bosbi,
        user_id: userJosh.id,
      },
      {
        first_name: "Josh",
        last_name: "Allen",
        text: "Had a great time at Chops. Our server Dane was super friendly. Dinner was delicious as always.",
        rating: 3,
        restaurant_id: roymed,
        user_id: userJosh.id,
      },
      {
        first_name: "Josh",
        last_name: "Allen",
        text: "The service was poor as we had to wait a long time for our food. There were some issues but were dealt with in a proper manner.",
        rating: 3,
        restaurant_id: cabi,
        user_id: userJosh.id,
      },
      {
        first_name: "Josh",
        last_name: "Allen",
        text: "Wonderful food and service.",
        rating: 5,
        restaurant_id: sude,
        user_id: userJosh.id,
      },
      {
        first_name: "Josh",
        last_name: "Allen",
        text: "Great food, great staff. You can’t ask for much more from a restaurant.",
        rating: 5,
        restaurant_id: mahe,
        user_id: userJosh.id,
      },
      {
        first_name: "LeBron",
        last_name: "James",
        text: "Wonderful service! Delicious food! Comfortable seating and luxurious atmosphere.",
        rating: 5,
        restaurant_id: muma,
        user_id: userLebron.id,
      },
      {
        first_name: "LeBron",
        last_name: "James",
        text: "Prime rib and filet were made spot on. Vegetable sides were made well as was the shrimp and scallops.",
        rating: 4,
        restaurant_id: aroa,
        user_id: userLebron.id,
      },
      {
        first_name: "LeBron",
        last_name: "James",
        text: "This visit was with a friend who had never been here before. She loved it as much as I do. She said it will be our new go to place!",
        rating: 4,
        restaurant_id: turde,
        user_id: userLebron.id,
      },
      {
        first_name: "LeBron",
        last_name: "James",
        text: "Had a full 3 course meal in the mid afternoon and every aspect of it was great. Server was named Brittany I believe and she was simply excellent.",
        rating: 5,
        restaurant_id: mezla,
        user_id: userLebron.id,
      },
      {
        first_name: "LeBron",
        last_name: "James",
        text: "Very nice evening spent with special family.",
        rating: 5,
        restaurant_id: safel,
        user_id: userLebron.id,
      },
      {
        first_name: "LeBron",
        last_name: "James",
        text: "First time, and not the last. Very welcoming. The food was deliscious and service very good. Highly recommend.",
        rating: 4,
        restaurant_id: marspi,
        user_id: userLebron.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "Enjoyed our drinks, dinner and dessert. Great service and ambience.",
        rating: 5,
        restaurant_id: tajmabi,
        user_id: userCassidy.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "The food was absolutely on point, we had such a great experience and our server was top notch. ",
        rating: 4,
        restaurant_id: bosbi,
        user_id: userCassidy.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "The steaks were 'Melt In Your Mouth'!!! Nigel, our waiter was amazing!! Great experience overall!",
        rating: 5,
        restaurant_id: roymed,
        user_id: userCassidy.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "It was really great! Just temperature wise it was really chilly. A little mixup at the end with desserts also but overall we really enjoyed the evening",
        rating: 4,
        restaurant_id: cabi,
        user_id: userCassidy.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "Food was served cold. Major No No. Fantastic Dessert. Service was good. Heavy Rock music should be toned down. Price vs Quality… not great.",
        rating: 3,
        restaurant_id: sude,
        user_id: userCassidy.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "Fantastic food and excellent selection. Everything was fresh - and the scones were still warm!",
        rating: 4,
        restaurant_id: mahe,
        user_id: userCassidy.id,
      },
      {
        first_name: "Cassidy",
        last_name: "Mancher",
        text: "Fantastic food and excellent selection. Everything was fresh - and the scones were still warm!",
        rating: 4,
        restaurant_id: muma,
        user_id: userCassidy.id,
      },
    ],
  });

  res.status(200).json({ name: "User & Reviews are Successfully Seeded" });
}
