// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import prisma from "@/db";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  await prisma.item.deleteMany();

  const restaurants = await prisma.restaurant.findMany();

  const sude =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "sultans-delight-savory-turkish-cuisine",
    )?.id || 1;
  const mahe =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "masala-haven-aromatic-indian-delights",
    )?.id || 1;
  const muma =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "mughlai-magic-exquisite-mughlai-cuisine",
    )?.id || 1;
  const aroa =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "arabian-oasis-authentic-middle-eastern-flavors",
    )?.id || 1;
  const turde =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "turkish-delight-culinary-journey-to-turkey",
    )?.id || 1;
  const mezla =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "mezze-lane-authentic-mediterranean-fare",
    )?.id || 1;
  const safel =
    restaurants.find(
      (restaurant) => restaurant.slug === "saffron-elegance-persian-gastronomy",
    )?.id || 1;
  const marspi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "marrakesh-spice-vibrant-moroccan-flavors",
    )?.id || 1;
  const tajmabi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "taj-mahal-bites-fusion-indian-cuisine",
    )?.id || 1;
  const bosbi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "bosphorus-bites-authentic-turkish-fare",
    )?.id || 1;
  const roymed =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "royal-medina-elegant-moroccan-cuisine",
    )?.id || 1;
  const cabi =
    restaurants.find(
      (restaurant) =>
        restaurant.slug === "cairo-bistro-egyptian-culinary-journey",
    )?.id || 1;

  await prisma.item.createMany({
    data: [
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Biryani_Delight",
        name: "Biryani Delight",
        description:
          "Fragrant basmati rice cooked with aromatic spices and tender halal chicken, garnished with fried onions and fresh mint.",
        price: "$20.00",
        restaurant_id: aroa,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Shawarma_Platter",
        name: "Shawarma Platter",
        description:
          "Sliced tender beef or chicken wrapped in warm pita bread with fresh veggies and drizzled with tangy garlic sauce.",
        price: "$16.50",
        restaurant_id: turde,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Kebab_Sampler",
        name: "Kebab Sampler",
        description:
          "Assortment of succulent grilled kebabs - lamb, chicken, and beef, seasoned with exotic spices.",
        price: "$22.50",
        restaurant_id: bosbi,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Tagine_Royale",
        name: "Tagine Royale",
        description:
          "Slow-cooked halal lamb with dried fruits and aromatic spices, served in an authentic Moroccan tagine.",
        price: "$25.00",
        restaurant_id: roymed,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Falafel_Platter",
        name: "Falafel Platter",
        description:
          "Crispy chickpea patties served with hummus, tahini, and a side of fresh salad.",
        price: "$14.00",
        restaurant_id: mezla,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Koshari_Bowl",
        name: "Koshari Bowl",
        description:
          "A classic Egyptian comfort dish with rice, lentils, chickpeas, pasta, and spicy tomato sauce.",
        price: "$12.00",
        restaurant_id: cabi,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Persian_Saffron_Rice",
        name: "Persian Saffron Rice",
        description:
          "Steamed aromatic basmati rice infused with premium saffron strands and buttery sautéed nuts.",
        price: "$18.00",
        restaurant_id: safel,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Mansaf_Platter",
        name: "Mansaf Platter",
        description:
          "Traditional Jordanian dish with tender lamb cooked in a tangy yogurt sauce, served with rice.",
        price: "$28.00",
        restaurant_id: aroa,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Mezze_Platter",
        name: "Mezze Platter",
        description:
          "An assortment of small dishes including hummus, baba ganoush, and falafel, perfect for sharing.",
        price: "$19.50",
        restaurant_id: mezla,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Kanafeh_Dessert",
        name: "Kanafeh Dessert",
        description:
          "Sweet and cheesy Middle Eastern dessert pastry soaked in sugar syrup and topped with pistachios.",
        price: "$8.00",
        restaurant_id: aroa,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Turkish_Delight_Baklava",
        name: "Turkish Delight Baklava",
        description:
          "Layers of phyllo pastry filled with nuts and sweetened with honey syrup, a classic Turkish dessert.",
        price: "$10.00",
        restaurant_id: sude,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Egyptian_Kushari",
        name: "Egyptian Kushari",
        description:
          "A popular Egyptian street food dish made with rice, macaroni, lentils, chickpeas, and tangy tomato sauce.",
        price: "$13.00",
        restaurant_id: cabi,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Moroccan_Harira_Soup",
        name: "Moroccan Harira Soup",
        description:
          "A hearty Moroccan soup made with tomatoes, lentils, chickpeas, and spices, a staple during Ramadan.",
        price: "$9.00",
        restaurant_id: roymed,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Chicken_Shawarma_Wrap",
        name: "Chicken Shawarma Wrap",
        description:
          "Marinated chicken wrapped in lavash bread with garlic sauce and pickles, a beloved Arabian street food.",
        price: "$12.00",
        restaurant_id: mahe,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Indian_Paneer_Tikka",
        name: "Indian Paneer Tikka",
        description:
          "Cubes of paneer marinated in yogurt and spices, grilled to perfection in a tandoor.",
        price: "$15.00",
        restaurant_id: muma,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Persian_Joojeh_Kebab",
        name: "Persian Joojeh Kebab",
        description:
          "Succulent marinated chicken skewers grilled to smoky perfection, a traditional Persian delight.",
        price: "$17.00",
        restaurant_id: safel,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Arabic_Kabsa_Rice",
        name: "Arabic Kabsa Rice",
        description:
          "Fragrant rice cooked with Arabic spices and tender halal meat, a favorite in Arabian cuisine.",
        price: "$19.00",
        restaurant_id: aroa,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Turkish_Manti",
        name: "Turkish Manti",
        description:
          "Small dumplings filled with seasoned meat, served with yogurt sauce and garlic, a Turkish delight.",
        price: "$16.00",
        restaurant_id: bosbi,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Moroccan_Pastilla",
        name: "Moroccan Pastilla",
        description:
          "Layers of crispy pastry filled with spiced meat, almonds, and dusted with powdered sugar.",
        price: "$21.00",
        restaurant_id: marspi,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Arabian_Umm_Ali",
        name: "Arabian Umm Ali",
        description:
          "A delightful Egyptian bread pudding with milk, nuts, and raisins, a comforting dessert.",
        price: "$11.00",
        restaurant_id: aroa,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Arabian_Shawarma_Plate",
        name: "Arabian Shawarma Plate",
        description:
          "Succulent beef or chicken wrapped in pita bread with tahini sauce and fresh vegetables.",
        price: "$14.00",
        restaurant_id: aroa,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Turkish_Lahmacun",
        name: "Turkish Lahmacun",
        description:
          "Thin Turkish flatbread topped with spiced ground meat, onions, and herbs, a Turkish pizza.",
        price: "$12.00",
        restaurant_id: turde,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Moroccan_Couscous_Royale",
        name: "Moroccan Couscous Royale",
        description:
          "A flavorful dish of steamed couscous served with a medley of vegetables and tender meat.",
        price: "$18.00",
        restaurant_id: roymed,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Indian_Butter_Chicken",
        name: "Indian Butter Chicken",
        description:
          "Tender chicken cooked in a rich, creamy tomato-based sauce with aromatic spices.",
        price: "$16.00",
        restaurant_id: muma,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Egyptian_Fattah",
        name: "Egyptian Fattah",
        description:
          "A traditional Egyptian dish made with rice, bread, and meat, topped with garlic tomato sauce.",
        price: "$13.00",
        restaurant_id: cabi,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Persian_Ghormeh_Sabzi",
        name: "Persian Ghormeh Sabzi",
        description:
          "A Persian stew made with slow-cooked herbs, kidney beans, and tender chunks of meat.",
        price: "$20.00",
        restaurant_id: mezla,
      },
      {
        image: "https://placehold.co/227x128/6EAF0F/white?text=Arabian_Kunafa",
        name: "Arabian Kunafa",
        description:
          "A delectable Middle Eastern dessert made of shredded phyllo dough, cheese, and sweet syrup.",
        price: "$10.00",
        restaurant_id: aroa,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Turkish_Baklava_Ice_Cream",
        name: "Turkish Baklava Ice Cream",
        description:
          "Creamy Turkish ice cream served with layers of baklava and drizzled with honey syrup.",
        price: "$15.00",
        restaurant_id: sude,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Moroccan_Kefta_Tagine",
        name: "Moroccan Kefta Tagine",
        description:
          "Savory Moroccan meatballs cooked in a spiced tomato-based sauce, served in a tagine.",
        price: "$17.00",
        restaurant_id: marspi,
      },
      {
        image:
          "https://placehold.co/227x128/6EAF0F/white?text=Indian_Samosa_Chaat",
        name: "Indian Samosa Chaat",
        description:
          "Crispy samosas topped with yogurt, chutney, and spices, a popular Indian street food.",
        price: "$9.00",
        restaurant_id: tajmabi,
      },
    ],
  });

  res.status(200).json({ name: "Item Successfully Seeded" });
}
