// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from "next";
import { PRICE } from "@prisma/client";
import prisma from "@/db";

type Data = {
  name: string;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>,
) {
  await prisma.restaurant.deleteMany();

  const locations = await prisma.location.findMany();
  const cuisines = await prisma.cuisine.findMany();

  const Surabaya =
    locations.find((location) => location.name === "Surabaya")?.id || 1;
  const Malang =
    locations.find((location) => location.name === "Malang")?.id || 1;
  const Jember =
    locations.find((location) => location.name === "Jember")?.id || 1;
  const Banyuwangi =
    locations.find((location) => location.name === "Banyuwangi")?.id || 1;
  const Bondowoso =
    locations.find((location) => location.name === "Bondowoso")?.id || 1;
  const Probolinggo =
    locations.find((location) => location.name === "Probolinggo")?.id || 1;
  const Lumajang =
    locations.find((location) => location.name === "Lumajang")?.id || 1;
  const Situbondo =
    locations.find((location) => location.name === "Situbondo")?.id || 1;
  const Pacitan =
    locations.find((location) => location.name === "Pacitan")?.id || 1;
  const Ponorogo =
    locations.find((location) => location.name === "Ponorogo")?.id || 1;

  const Indian = cuisines.find((cuisine) => cuisine.name === "Indian")?.id || 1;
  const Arabian =
    cuisines.find((cuisine) => cuisine.name === "Arabian")?.id || 1;
  const Turkish =
    cuisines.find((cuisine) => cuisine.name === "Turkish")?.id || 1;
  const Moroccan =
    cuisines.find((cuisine) => cuisine.name === "Moroccan")?.id || 1;
  const Persian =
    cuisines.find((cuisine) => cuisine.name === "Persian")?.id || 1;
  const Egyptian =
    cuisines.find((cuisine) => cuisine.name === "Egyptian")?.id || 1;

  await prisma.restaurant.createMany({
    data: [
      {
        name: "Sultan's Delight - Savory Turkish",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=sultans_delight",
        price: PRICE.CHEAP,
        description:
          "Sultan's Delight brings the taste of Turkey with its rich and flavorful dishes. From kebabs to Turkish delights, indulge in a culinary journey through the diverse Turkish cuisine.",
        open_time: "12:00:00.000Z",
        close_time: "20:00:00.000Z",
        slug: "sultans-delight-savory-turkish-cuisine",
        location_id: Surabaya,
        cuisine_id: Turkish,
      },
      {
        name: "Masala Haven - Aromatic Indian Delights",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=masala_haven",
        price: PRICE.REGULAR,
        description:
          "Masala Haven welcomes you to savor the aromatic spices of India. From creamy curries to sizzling tandoori, indulge in the rich flavors of Indian cuisine.",
        open_time: "11:30:00.000Z",
        close_time: "22:00:00.000Z",
        slug: "masala-haven-aromatic-indian-delights",
        location_id: Malang,
        cuisine_id: Indian,
      },
      {
        name: "Mughlai Magic - Exquisite Mughlai Cuisine",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=mughlai_magic",
        price: PRICE.EXPENSIVE,
        description:
          "Mughlai Magic offers the royal flavors of Mughlai cuisine. Delight in aromatic biryanis and rich creamy gravies, reminiscent of the Mughal era.",
        open_time: "12:00:00.000Z",
        close_time: "23:00:00.000Z",
        slug: "mughlai-magic-exquisite-mughlai-cuisine",
        location_id: Jember,
        cuisine_id: Indian,
      },
      {
        name: "Arabian Oasis - Authentic Middle Eastern Flavors",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=arabian_oasis",
        price: PRICE.REGULAR,
        description:
          "Arabian Oasis invites you to experience the richness of Middle Eastern flavors. From succulent kebabs to fragrant rice dishes, savor the taste of Arabia.",
        open_time: "13:00:00.000Z",
        close_time: "22:30:00.000Z",
        slug: "arabian-oasis-authentic-middle-eastern-flavors",
        location_id: Banyuwangi,
        cuisine_id: Arabian,
      },
      {
        name: "Turkish Delight - Culinary Journey to Turkey",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=turkish_delight",
        price: PRICE.REGULAR,
        description:
          "Turkish Delight offers a culinary journey to Turkey. Explore the diverse flavors from kebabs to baklava, and experience the essence of Turkish cuisine.",
        open_time: "12:30:00.000Z",
        close_time: "21:00:00.000Z",
        slug: "turkish-delight-culinary-journey-to-turkey",
        location_id: Bondowoso,
        cuisine_id: Turkish,
      },
      {
        name: "Mezze Lane - Authentic Mediterranean Fare",
        main_image: "https://placehold.co/227x128/6EAF0F/white?text=mezze_lane",
        price: PRICE.REGULAR,
        description:
          "Mezze Lane brings the authentic flavors of the Mediterranean. From mezze platters to flavorful shawarmas, embark on a gastronomic journey.",
        open_time: "11:00:00.000Z",
        close_time: "22:30:00.000Z",
        slug: "mezze-lane-authentic-mediterranean-fare",
        location_id: Probolinggo,
        cuisine_id: Persian,
      },
      {
        name: "Saffron Elegance - Persian Gastronomy",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=saffron_elegance",
        price: PRICE.EXPENSIVE,
        description:
          "Saffron Elegance offers the opulence of Persian cuisine. Indulge in saffron-infused delicacies and aromatic stews for a taste of Persia.",
        open_time: "12:00:00.000Z",
        close_time: "22:30:00.000Z",
        slug: "saffron-elegance-persian-gastronomy",
        location_id: Lumajang,
        cuisine_id: Persian,
      },
      {
        name: "Marrakesh Spice - Vibrant Moroccan Flavors",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=marrakesh_spice",
        price: PRICE.REGULAR,
        description:
          "Marrakesh Spice brings the vibrant flavors of Morocco. Experience the blend of spices in tagines and couscous, transporting you to Marrakesh.",
        open_time: "11:30:00.000Z",
        close_time: "21:00:00.000Z",
        slug: "marrakesh-spice-vibrant-moroccan-flavors",
        location_id: Situbondo,
        cuisine_id: Moroccan,
      },
      {
        name: "Taj Mahal Bites - Fusion Indian Cuisine",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=taj_mahal_bites",
        price: PRICE.REGULAR,
        description:
          "Taj Mahal Bites offers a fusion of traditional and modern Indian cuisine. Enjoy innovative dishes inspired by flavors from the Taj Mahal.",
        open_time: "12:00:00.000Z",
        close_time: "22:00:00.000Z",
        slug: "taj-mahal-bites-fusion-indian-cuisine",
        location_id: Pacitan,
        cuisine_id: Indian,
      },
      {
        name: "Bosphorus Bites - Authentic Turkish Fare",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=bosphorus_bites",
        price: PRICE.REGULAR,
        description:
          "Bosphorus Bites brings authentic Turkish flavors to your table. Immerse yourself in the taste of Istanbul with savory kebabs and sweet baklava.",
        open_time: "13:00:00.000Z",
        close_time: "23:00:00.000Z",
        slug: "bosphorus-bites-authentic-turkish-fare",
        location_id: Ponorogo,
        cuisine_id: Turkish,
      },
      {
        name: "Royal Medina - Elegant Moroccan Cuisine",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=royal_medina",
        price: PRICE.EXPENSIVE,
        description:
          "Royal Medina offers an elegant experience with Moroccan cuisine. From aromatic tagines to traditional couscous, indulge in royal Moroccan flavors.",
        open_time: "12:30:00.000Z",
        close_time: "22:30:00.000Z",
        slug: "royal-medina-elegant-moroccan-cuisine",
        location_id: Surabaya,
        cuisine_id: Moroccan,
      },
      {
        name: "Cairo Bistro - Egyptian Culinary Journey",
        main_image:
          "https://placehold.co/227x128/6EAF0F/white?text=cairo_bistro",
        price: PRICE.REGULAR,
        description:
          "Cairo Bistro offers an enchanting journey through Egyptian cuisine. Delight in flavors from the Nile, savoring authentic dishes from Cairo's bustling streets.",
        open_time: "12:00:00.000Z",
        close_time: "22:00:00.000Z",
        slug: "cairo-bistro-egyptian-culinary-journey",
        location_id: Malang,
        cuisine_id: Egyptian,
      },
    ],
  });

  res.status(200).json({ name: "Restaurant Date Successfully Seeded" });
}
